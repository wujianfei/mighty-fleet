<?php

namespace Fleet\Vessel;

interface VesselInterface
{
    public function moveToLocation(Location $location);

    public function getType();

    public function getId();

    public function getLocation();
}