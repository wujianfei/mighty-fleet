<?php

namespace Fleet\Vessel;

class Location
{
    /**
     * @var string
     */
    private $id;

    /**
     * location on X axis
     * @var int
     */
    private $xCoord;

    /**
     * location on Y axis
     * @var int
     */
    private $yCoord;

    public function __construct($xCoord, $yCoord)
    {
        $this->id = $xCoord . '-' . $yCoord;
        $this->xCoord = $xCoord;
        $this->yCoord = $yCoord;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setXCoord($xCoord)
    {
        $this->xCoord = $xCoord;
    }

    public function setYCoord($yCoord)
    {
        $this->yCoord = $yCoord;
    }

    public function getXCoord()
    {
        return $this->xCoord;
    }

    public function getYCoord()
    {
        return $this->yCoord;
    }
}