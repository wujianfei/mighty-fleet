<?php

namespace Fleet\Vessel;

abstract class Vessel implements VesselInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var Location
     */
    private $location;

    public function __construct($location = null)
    {
        $this->id = uniqid();
        $this->location = $location;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getLocation()
    {
        return $this->location;
    }

    public function moveToLocation(Location $location)
    {
        $this->location = $location;
    }

    abstract public function getType();
}