<?php

namespace Fleet\Vessel\Type\SupportCraft;

use Fleet\Vessel\Vessel;

abstract class AbstractSupportCraft extends Vessel implements SupportCraftInterface
{
    protected $medicalUnit;

    public function __construct($medicalUnit)
    {
        parent::__construct();
        $this->medicalUnit = $medicalUnit;
    }

    public function getMedicalUnit()
    {
        return $this->medicalUnit;
    }

    public function setMedicalUnit($medicalUnit)
    {
        $this->medicalUnit = $medicalUnit;
    }
}