<?php

namespace Fleet\Vessel\Type\SupportCraft;

class MechanicalVessel extends AbstractSupportCraft
{
    public function getType()
    {
        return 'Mechanical';
    }
}