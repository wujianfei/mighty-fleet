<?php

namespace Fleet\Vessel\Type\SupportCraft;

class CargoVessel extends AbstractSupportCraft
{
    public function getType()
    {
        return 'Cargo';
    }
}