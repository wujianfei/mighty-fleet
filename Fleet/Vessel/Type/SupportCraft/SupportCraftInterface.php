<?php

namespace Fleet\Vessel\Type\SupportCraft;

use Fleet\Vessel\VesselInterface;

interface SupportCraftInterface extends VesselInterface
{
    public function getMedicalUnit();

    public function setMedicalUnit($medicalUnit);
}