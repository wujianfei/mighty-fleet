<?php

namespace Fleet\Vessel\Type\SupportCraft;

class RefuelingVessel extends AbstractSupportCraft
{
    public function getType()
    {
        return 'Refueling';
    }
}