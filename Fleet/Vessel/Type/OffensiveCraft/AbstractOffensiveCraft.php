<?php

namespace Fleet\Vessel\Type\OffensiveCraft;

use Fleet\Vessel\Vessel;

abstract class AbstractOffensiveCraft extends Vessel implements OffensiveCraftInterface
{
    /**
     * @var int
     */
    protected $cannons;

    public function __construct($cannons = 0)
    {
        parent::__construct();
        $this->cannons = $cannons;
    }
    
    /**
     * Fire off all cannons
     */
    public function attack()
    {
        $this->cannons = 0;
    }

    /**
     * Raise shields
     */
    public function raiseShields()
    {
        // do something
    }
}