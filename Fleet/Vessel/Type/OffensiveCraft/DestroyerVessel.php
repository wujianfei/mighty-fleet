<?php

namespace Fleet\Vessel\Type\OffensiveCraft;

class DestroyerVessel extends AbstractOffensiveCraft
{
    public function __construct()
    {
        parent::__construct();
        $this->cannons = 12;
    }

    public function getType()
    {
        return 'Destroyer';
    }
}