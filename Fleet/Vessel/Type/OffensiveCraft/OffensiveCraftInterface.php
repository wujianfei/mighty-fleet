<?php

namespace Fleet\Vessel\Type\OffensiveCraft;

use Fleet\Vessel\VesselInterface;

interface OffensiveCraftInterface extends VesselInterface
{
    public function attack();

    public function raiseShields();
}