<?php

namespace Fleet\Vessel\Type\OffensiveCraft;

class CruiserVessel extends AbstractOffensiveCraft
{
    public function __construct()
    {
        parent::__construct();
        $this->cannons = 6;
    }

    public function getType()
    {
        return 'Cruiser';
    }
}