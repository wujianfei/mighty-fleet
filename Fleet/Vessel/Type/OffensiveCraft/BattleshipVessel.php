<?php

namespace Fleet\Vessel\Type\OffensiveCraft;

class BattleshipVessel extends AbstractOffensiveCraft
{
    public function __construct()
    {
        parent::__construct();
        $this->cannons = 24;
    }

    public function getType()
    {
        return 'Battership';
    }
}