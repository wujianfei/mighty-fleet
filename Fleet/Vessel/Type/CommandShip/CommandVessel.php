<?php

namespace Fleet\Vessel\Type\CommandShip;

use Fleet\Vessel\Type\OffensiveCraft\BattleshipVessel;
use Fleet\MyFleet;

class CommandVessel extends BattleshipVessel
{
    private $fleet;

    public function __construct(MyFleet $fleet)
    {
        parent::__construct();

        $this->fleet = $fleet;
    }

    public function getType()
    {
        return 'Command';
    }
}