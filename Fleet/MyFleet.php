<?php

namespace Fleet;

use Fleet\Vessel\Type\CommandShip\CommandVessel;
use Fleet\Vessel\VesselInterface;

class MyFleet
{
    /**
     * @var array
     */
    private $vessels;

    /**
     * @var CommandShipType
     */
    private $commandShip;

    public function __construct(array $vessels, CommandVessel $commandShip)
    {
        if (count($vessels) > 50) {
            throw new \Exception("You can't have a fleet with more than 50 vessels.");
        }

        $this->vessels = $vessels;
        $this->commandShip = $commandShip;
    }

    public function addVessel(VesselInterface $vessel)
    {
        if (count($this->vessels) === 50) {
            throw new \Exception("The fleet already has 50 vessels");
        }

        $this->vessels[$vessel->getId()] = $vessel;
    }

    public function getVesselById($id)
    {
        return isset($this->vessels[$id]) ? $this->vessels[$id] : null;
    }

    public function getCommandShip()
    {
        return $this->commandShip;
    }

    public function setCommandShip(CommandVessel $commandShip)
    {
        $this->commandShip = $commandShip;
    }
}