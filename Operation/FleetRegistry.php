<?php

namespace Operation;

use Fleet\Vessel\Type\OffensiveCraft\BattleshipVessel;
use Fleet\Vessel\Type\SupportCraft\RefuelingVessel;
use Fleet\Vessel\VesselInterface;
use Fleet\Vessel\Location;
use Fleet\Vessel\Type\OffensiveCraft\OffensiveCraftInterface;
use Fleet\Vessel\Type\SupportCraf\SupportCraftInterface;

class FleetRegistry
{
    private $locations = array();

    private $vessels = array();

    private $supportVessels = array();

    private $offensiveVessels = array();

    public function __construct()
    {
        // construct equal number of support (refueling) and offensive (battleship) vessels
        for ($num=0; $num < 25; $num++) {
            // battleship
            $offensiveVessel = new BattleshipVessel;
            $this->deployVessel($offensiveVessel);

            // refueling, passing in a generic object representing a medical unit
            $supportVessel = new RefuelingVessel(new \stdClass);
            $this->deployVessel($supportVessel);
        }
    }

    /**
     * Find a spot in 100x100 grid which hasn't been taken
     * 
     * @param  VesselInterface $vessel
     */
    public function deployVessel(VesselInterface $vessel)
    {
        $location = new Location(rand(0, 100), rand(0, 100));

        while (isset($this->locations[$location->getId()])) {
            $location = new Location(rand(0, 100), rand(0, 100));
        }

        $vessel->moveToLocation($location);

        $this->locations[$location->getId()] = $location;
        $this->vessels[$vessel->getId()] = $vessel;

        if ($vessel instanceof OffensiveCraftInterface) {
            $this->offensiveVessels[$vessel->getId()] = $vessel;
        }

        if ($vessel instanceof SupportCraftInterface) {
            $this->supportVessels[$vessel->getId()] = $vessel;
        }
    }
}